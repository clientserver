import java.net.*;
import java.util.*;
import java.io.*;

/**
 * Client, connects to a Server and acts as the
 * user interface with which users can chat with
 * other users on the same server and in the same
 * chatroom.
 *
 * TODO: add run-time support to change the port
 *       used to send messages to the server.
 *
 * @author "Victor Zamanian" <victor.zamanian@gmail.com>
 * Time-stamp: "Tue 11 Sep 2007 11:15:49 AM CEST"
 */
public class Client {

    // The username for the user of this client
    private String nickName = null;
    // The host to connect to
    private InetAddress hostAddress = null;
    // The port to connect on
    private int port;
    // The socket to connect with
    private DatagramSocket clientSocket = null;

    /**
     * Constructor for Client, creates a socket
     * to send and receive data through.
     */
    public Client() {
        // Create socket
        try {
            this.clientSocket = new DatagramSocket();
        } catch (SocketException e) {
            System.err.println(e);
        }
    }

    /**
     * getHelp returns the help text that describes
     * how to use the different commands and what they do.
     *
     * return String helpString
     */
    public String getHelp() {

        return "Use \'/\' in front of a command word " +
            "to invoke that command.\n" +
            "Commands are as follows:\n\n" +

            "help:      display this help message.\n" +

            "login:     login to a server with a nickname.\n" +
            "login new: login to another server (should only be used if\n" +
            "           already logged in to a server).\n" +
            
            "lschans:   list the channels (chat rooms) that\n" +
            "           have been created on the server.\n" +

            "lsusers:   list the users that have currently joined\n" +
            "           the same channel as you have.\n" +

            "createchannel channelName: create a channel on the server.\n" +

            "join channelName: join a channel on the server.\n" +

            "leave:   leave the channel you are in.\n" +

            "quit:    quit (exit) the Client.\n"
            // doesn't seem to work on all terminal sessions.
            /*
              +
              "clear:   clear the screen of text (or at least " +
              "move it up a bit)."
            */
            ;
    }

    // GET METHODS

    /**
     * getNickName returns the nickname that
     * this user is logged in with on the server.
     *
     * @return String nickname
     */
    public String getNickName() {
        return this.nickName;
    }
    
    /**
     * getHost returns the InetAddress that
     * the server has.
     *
     * @return InetAddress hostAddress
     */
    public InetAddress getHost() {
        return this.hostAddress;
    }

    /**
     * getPort returns the port the server listens to
     * for new messages from users.
     *
     * @return int port
     */
    public int getPort() {
        return this.port;
    }

    /**
     * getSocket returns the DatagramSocket that this
     * client uses to send and receive messages.
     *
     * @return DatagramSocket clientSocket
     */
    public DatagramSocket getSocket() {
        return this.clientSocket;
    }

    // SET methods

    /**
     * setNickName sets this client's nickname.
     * This method isn't really to be used other
     * than when loggin into the server. No support
     * exists (yet) for changing one's nickname once
     * logged into the server.
     *
     * @param String nickName
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * setHost sets the host (server IP address)
     * that this client is connected to.
     *
     * @param InetAddress host
     */
    public void setHost(InetAddress host) {
        this.hostAddress = host;
    }

    /**
     * setPort sets the port which the server, which
     * this client is connected to, listens on for
     * incoming messages.
     *
     * @param int port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * setSocket sets the socket which this client
     * uses to send and receive messages to a new socket.
     *
     * @param DatagramSocket socket
     */
    public void setSocket(DatagramSocket socket) {
        // close current socket
        this.clientSocket.close();
        // switch over to new socket
        this.clientSocket = socket;
    }

    /**
     * login prompts the user for a nickname
     * and information about the server to login to.
     */
    public void login() throws IOException {

        // temporary variables to help the process
        String tempInput = null;

        // Prompt for the nickname to use at login
        do {
            tempInput = Library.prompt("Enter your desired username: ");
        } while (tempInput.length() < 1 || tempInput.length() > 255);
        // while the nickname is not the empty String and is not longer
        // than 255 characters (the max. size of a byte)

        // set this client's nickname to what the user put in
        this.setNickName(tempInput);

        // Prompt for the host (server) to connect to
        tempInput = Library.prompt("Establish connection to: ");
        try {
            this.setHost(InetAddress.getByName(tempInput));
        }
        catch (UnknownHostException e) {
            System.err.println(e);
        }
        
        // Prompt for the port used by the server
        this.setPort(Library.promptInt("Connect on port no.: "));

        // create a new DatagramPacket of type 1
        // (login to server) to send to the server.
        DatagramPacket outPacket = this.constructPacket(1, this.getNickName());

        // send the packet
        try {
            this.getSocket().send(outPacket);
        }
        catch (IOException e) {
            System.err.println(e);
        }
    }

    /**
     * constructPacket is a method that builds a DatagramPacket
     * depending on what is sent to the method as parameters.
     * It handles length of the message and inserts it correctly
     * into the byte array in the packet, as well as making sure
     * the message length does not cause the packet to fill up
     * past its defined capacity (255 characters).
     *
     * @param int messageType
     * @param String message
     */
    public DatagramPacket constructPacket(int messageType, String message) {
        byte[] packetData = new byte[257];
        String localMessage = message;

        if (localMessage == null) {
            localMessage = "";
        }

        // set the message type for the packet
        packetData[0] = (byte) messageType;
        // set the length of the packet. (255 if localMessage >= 255)
        packetData[1] = (localMessage.length() < 255) ? (byte) localMessage.length() : (byte) 255;

        // put the message into the packet data
        for (int i=0; i<packetData[1]; i++) {
            packetData[2+i] = (byte) localMessage.charAt(i);
        }

        // make a new DatagramPacket and return it
        return new DatagramPacket(
                                  packetData,
                                  packetData.length,
                                  this.getHost(),
                                  this.getPort()
                                  );
    }


    // main. What to say... :P
    public static void main(String[] args) throws Exception {

        // create a new instance of Client
        Client client = new Client();
        // boolean keepLoopRunning helps
        // keep track of whether or not the
        // program should terminate once it goes
        // back into the infinite "input-loop".
        boolean keepLoopRunning = true;

        // Login to the server
        client.login();

        // Start thread for receiving messages from the server
        new ReceiveMessageThread(client.getSocket()).start();


        // START HANDLING INPUT FROM THE USER AND SENDING MESSAGES

        // Fields needed within the loop
        byte[] sendData = null;
        String input = null;
        DatagramPacket outPacket = null;
        int messageType = 0;

        // input-loop
        while (keepLoopRunning) {

            // Read from keyboard
            // while the user entered the empty String
            input = "";
            while (input.length() < 1) {
                input = Library.prompt();
                // fixes a "bad thing" where a user can
                // clear the screen of any other user
                // in the same chat room as this
                // user (and the server).
                // UPDATE: seems to only affect putty sessions?
                if (input.contains("\f")) {
                    System.out.println("Message cannot contain a line feed (\'\\f\'/^L)");
                    input = "";
                    // start over 
                    continue;
                }
            }

            // If a command was invoked ('/')
            if (input.charAt(0) == '/') {

                // String for saving the invoked command
                // (we will use 'input' to save any
                // potential parameter)
                String command = null;

                StringTokenizer tokens = new StringTokenizer(input.substring(1, input.length()));
                // if just "/" was typed
                if (tokens.countTokens() == 0) {
                    // Start loop over again
                    continue;
                }
                // if a command was invoked that takes no parameters.
                else if (tokens.countTokens() == 1) {
                    command = tokens.nextToken();
                    // set input to null, because when we construct the
                    // DatagramPacket later on we want the length of the
                    // packet message to be zero.
                    input = null;

                    if (command.equals("login")) {
                        messageType = 1;
                        input = client.getNickName();
                        System.out.println("Login with nickname: " + input);
                    }
                    else if (command.equals("lschans")) {
                        messageType = 3;
                        System.out.println("Channels: ");
                    }
                    else if (command.equals("lsusers")) {
                        messageType = 6;
                        System.out.println("Users: ");
                    }
                    else if (command.equals("quit")) {
                        messageType = 7;
                        System.out.println("User chose to quit.");
                        keepLoopRunning = false;
                    }
                    else if (command.equals("leave")) {
                        messageType = 7;
                    }
                    else if (command.equals("help")) {
                        // Print help about commands
                        System.out.println('\n' + client.getHelp() + '\n');
                        // start loop over (don't send a message to server)
                        continue;
                    }
//                     else if (command.equals("clear")) {
//                         // move text up ("clear screen")
//                         System.out.print('\f');
//                         continue;
//                     }
                    else {
                        System.out.println("Command syntax unknown: " +
                                           command);
                        continue;
                    }
                }
                // if a command was invoked that takes a parameter
                // (we do not pay attention to trailing parameters)
                else if (tokens.countTokens() > 1) {
                    command = tokens.nextToken();
                    // set input to be the parameter to the command invoked
                    // that way it will be put as the message of the byte array
                    // in the constructed DatagramPacket later on
                    input = tokens.nextToken();

                    if (command.equals("createchannel")) {
                        messageType = 2;
                    }
                    else if (command.equals("login") && input.equals("new")) {
                        client.login();
                    }
                    else if (command.equals("join")) {
                        messageType = 4;
                    }
                    else {
                        System.out.println("Command syntax unknown: " +
                                           command + ' ' + input);
                        continue;
                    }
                }
            }
            // If no command invoked--regular message to channel
            else {
                messageType = 5;
            }

            // construct packet to send.
            // Here, input will have been altered depending
            // on whether or not a command was invoked, and
            // which command was potentially invoked.
            outPacket = client.constructPacket(messageType, input);

            // try sending the packet
            try {
                client.getSocket().send(outPacket);
            }
            catch (IOException e) {
                System.err.println(e);
            }

        } // end while

        // Close socket if loop is ended (user termination)
        client.getSocket().close();

    } // end main
}

/**
 * ReceiveMessageThread receives messages in the
 * background and prints them to standard output.
 */
class ReceiveMessageThread extends Thread {

    // store the packet data into this byte array
    byte[] receivedData = null;
    // datagram socket to use
    DatagramSocket clientSocket = null;
    // receive the packets with this datagram packet
    DatagramPacket inPacket = null;

    int messageType = 0;
    int messageLength = 0;
    String message = null;

    /**
     * Constructs a new instance of a ReceiveMessageThread.
     * The constructor takes a DatagramSocket as a parameter
     * and uses it to receive messages.
     *
     * @param DatagramSocket clientSocket
     */
    public ReceiveMessageThread(DatagramSocket clientSocket) {
        this.clientSocket = clientSocket;
        this.receivedData = new byte[257];
        this.inPacket = new DatagramPacket(receivedData, receivedData.length);
    }
    
    public void run() {

        // infinite loop (to keep this going)
        while (true) {
            // try to receive a packet
            try {
                clientSocket.receive(inPacket);
            }
            // if the socket was closed, we stop the thread
            catch (IOException e) {
                System.err.println("Socket was closed, now exiting...");
                // End the loop.
                break;
            }

            // fetch the message type (not really used)
            messageType = (int) receivedData[0] & 0xff;
            // fetch the length of the message in the data
            messageLength = (int) receivedData[1] & 0xff;

            message = new String(receivedData).substring(2, messageLength + 2);

            // make sure we're "not printing nothing"
            if (message == null || message.equals("")) {
                message = "Something went wrong here...";
            }
            // print message
            System.out.println(message);

        } // end infinite loop :-)
    } // end run()
} // end class.
