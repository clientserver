/*
 * @(#)TestServer.java
 * Time-stamp: "2007-09-18 10:48:44 anton"
 */

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.DatagramSocket;

/**
 * TestServer
 *
 * @author "Anton Johansson" <anton.johansson@gmail.com>
 */
public class TestServer {
    public TestServer() {
    }

    public static void main (String argv[]) throws Exception
    {
        // Skapa socket
        DatagramSocket clientSocket = new DatagramSocket();
        // Skapa IP-adress
        InetAddress IPAddress = InetAddress.getByName("localhost");
        // Läs in från tangentbordet
        byte[] sendData = new byte[1024];
        String message = "hej detta er meddelandet";
        sendData[0] = (byte) 2;
        sendData[1] = new Integer(message.length()).byteValue();
        for (int i = 0, n = message.length(); i < n; i++) {
            sendData[i+2] = (byte) message.charAt(i);
        }

        System.out.println("length -> " + sendData[1]);        
        // Skapa datagram med sänddata och sänd det
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 7877);
        clientSocket.send(sendPacket);

        clientSocket.close();
    }
}
