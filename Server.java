/*
 * @(#)Server.java
 * Time-stamp: "2007-10-04 09:53:29 anton"
 */

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Collection;
import java.util.Hashtable;

/**
 * Server
 *
 * @author "Anton Johansson" <anton.johansson@gmail.com>
 */
public class Server {
    private Hashtable<InetAddress, User> users;
    private Hashtable<String, Channel> channels;
    private DatagramSocket serverSocket;
    private static final byte USERADDED             = 101; // Användare inlagd Användarens namn Sändande klient
    private static final byte CHANNELCREATED        = 102; // Chatrum skapat Namnet på det nya chatrummet Alla klienter i alla rum
    private static final byte NAMEONCHANNEL         = 103; // Namn på ett chat-rum Namn Den som frågade efter en lista på rum
    private static final byte USERJOINEDCHANNEL     = 104; // Ny deltagare i ett rum Deltagarens namn Alla klienter i aktuellt rum
    private static final byte MESSAGETOALLINCHANNEL = 105; // Meddelande till alla i rummet Meddelandetext Alla klienter i aktuellt rum
    private static final byte NAMEONUSER            = 106; // Namn på en av deltagarna i rummet Deltagarens namn Den som frågade efter en lista
    private static final byte USERLEFTCHANNEL       = 107; // En deltagare har lämnat rummet Deltagarens namn Alla klienter i aktuellt rum
    private static final byte ERROR                 = 108; // Ett fel har uppstått.
    private static final String SERVER              = "MODERMODEMET";

    /**
     * Creates a new Server instance.
     *
     * @param serverPort the port the server is listening to
     * @exception SocketException if an error occurs
     * @exception IOException if an error occurs
     */
    public Server(int serverPort) throws SocketException, IOException {
        this.users = new Hashtable<InetAddress, User>();
        this.channels = new Hashtable<String, Channel>();
        this.serverSocket = new DatagramSocket(serverPort);

        byte[] sendData = new byte[1024];
        byte[] receivedData = new byte[1024];

        //checks for new messages from clients
        while(true) {
            DatagramPacket receivePacket = new DatagramPacket(receivedData,
                                                              receivedData.length);

            // receives messages from client
            serverSocket.receive(receivePacket);
            receivedData = receivePacket.getData();

            int messageType = receivedData[0];
            int messageLength = receivedData[1];
            String message = new String(receivedData).substring(2, messageLength + 2);
            InetAddress IPAddress = receivePacket.getAddress();
            int port = receivePacket.getPort();
            Channel channel;
            User user = users.get(IPAddress);

            System.out.println("=========> New Packet Received <=============");
            System.out.println("Got a request from " + IPAddress + ":" + port);
            System.out.println("Type : " + messageType);
            System.out.println("Message : " + message);

            if (user != null) {
                user.setPort(port);
            }
            else if (messageType != 1) {
                System.out.println("Got corrupt message from none user");
                sendMessage(ERROR, SERVER,
                            "You are not logged in",
                            IPAddress, port);
                continue;
            }

            if ((messageType == 1 || messageType == 2 || messageType == 4)
                && (message == null || message == "")) {
                //FIX error error achtung
                System.out.println("Got a request, type " + messageType + " with empty message");
                sendMessage(ERROR, SERVER,
                            "You didn't supply enough information",
                            user);
                continue;
            }

            switch (messageType) {
            case 1:
                //new user
                user = new User(message, IPAddress, port);
                users.put(user.getIPAddress(), user);
                sendMessage(USERADDED, SERVER,
                            "User " + message + " created",
                            user);
                break;
            case 2:
                //create channel
                if (channels.get(message) != null) {
                    System.out.println("Tried to create chanel " + message + " that already exists");
                    sendMessage(ERROR, SERVER,
                                "This channel already exists",
                                user);
                    continue;
                }
                channel = new Channel(message);
                channels.put(message, channel);
                sendMessageToAll(CHANNELCREATED, SERVER,
                                 user.getNick() + " created channel \"" + channel.getName() + "\"");
                //sendMessage(CHANNELCREATED, SERVER, "Channel " + message + " created", user);
                break;
            case 3:
                //list channels
                if (channels.isEmpty()) {
                    try {
                        sendMessage(ERROR, SERVER,
                                    "There are no channels",
                                    user);
                    }
                    catch (IOException e) {
                        System.out.println(e);
                    }
                    continue;
                }

                Collection<Channel> tempChannels = channels.values();
                for (Channel c : tempChannels) {
                    try {
                        sendMessage(USERADDED, SERVER, c.getName(), user);
                    }
                    catch (IOException e) {
                        System.out.println(e);
                    }
                }
                break;
            case 4:
                //enter channel
                System.out.println(user.getNick());
                channel = joinChannel(message, user);
                sendMessageToChannel(USERJOINEDCHANNEL, SERVER,
                                     "User " + user.getNick() + " joined \"" + channel.getName() + "\"",
                                     channel);
                break;
            case 5:
                //message to all in channel
                if (user.getChannel() == null) {
                    sendMessage(ERROR, SERVER,
                                "You are not in a channel",
                                user);
                    continue;
                }
                sendMessageToChannel(MESSAGETOALLINCHANNEL, user.getNick(),
                                     message, user.getChannel());
                break;
            case 6:
                //list users in channel
                if (user.getChannel() == null) {
                    sendMessage(ERROR, SERVER,
                                "You are not in a channel",
                                user);
                    continue;
                }
                channel = user.getChannel();
                //FIX Varför kan jag omdef users här?
                Collection<User> users = channel.getUsers();
                for (User u : users) {
                    sendMessage(NAMEONUSER, SERVER,
                                "User: " + u.getNick(),
                                user);
                }
                break;
            case 7:
                //leave channel
                if ((channel = user.getChannel()) == null) {
                    sendMessage(ERROR, SERVER,
                                "You are not in a channel",
                                user);
                    continue;
                }
                user.joinChannel(null);
                channel.leaveUser(user);
                sendMessage(USERLEFTCHANNEL, SERVER,
                            "You left the channel \"" + channel.getName() + "\"",
                            user);
                sendMessageToChannel(USERLEFTCHANNEL, SERVER,
                                     "User " + user.getNick() + " has left this channel",
                                     channel);
                break;
            default:
                System.out.println("Unknown messageType received");
                break;
            }
        }
    }

    /**
     * Joins a user to a channel. If there is no such channel on the
     * server, a new one is created.
     *
     * @param channelName the name of the channel to join
     * @param user the user who joins the channel
     * @return the channel joined and possibly newly created
     * @exception IOException if an error occurs
     */
    public Channel joinChannel(String channelName, User user) throws IOException {
        Channel channel = user.getChannel();
        if (channel != null) {
            channel.leaveUser(user);
            sendMessageToChannel(USERLEFTCHANNEL, SERVER,
                                 "User " + user.getNick() + " has left this channel",
                                 channel);
        }

        channel = channels.get(channelName);
        if (channel == null) {
            //create channel
            channel = new Channel(channelName);
            channels.put(channelName, channel);
            sendMessageToAll(CHANNELCREATED, SERVER,
                             user.getNick() + " created channel \"" + channel.getName() + "\"");
        }
        //join channel
        user.joinChannel(channel);
        channel.joinUser(user);
        return channel;
    }

    /**
     * Sends a message to everyone in a Channel
     *
     * @param messageType the type of the message
     * @param sender the sender of the message
     * @param message the message
     * @param channel the channel to send the message to
     * @exception IOException if an error occurs
     */
    public void sendMessageToChannel(byte messageType, String sender,
                                     String message,
                                     Channel channel) throws IOException {
        Collection<User> users = channel.getUsers();
        for (User toUser : users) {
            sendMessage(messageType, sender, message, toUser);
        }
    }

    /**
     * Sends a message to every user connected to this server
     *
     * @param messageType the type of the message
     * @param sender the sender of the message
     * @param message the message
     * @exception IOException if an error occurs
     */
    public void sendMessageToAll(byte messageType, String sender,
                                 String message) throws IOException {
        Collection<User> allUsers = users.values();
        for (User toUser : allUsers) {
            sendMessage(messageType, sender, message, toUser);
        }
    }

    /**
     * Sends a message to a specified IP-Address and port-number. Used
     * to send messages to users not correctly logged in to the
     * server.
     *
     * @param messageType the type of the message
     * @param sender the sender of the message
     * @param message the message
     * @param IPAddress the IP-Address to send the message to
     * @param port the port-number to send the message to
     * @exception IOException if an error occurs
     */
    public void sendMessage(byte messageType, String sender,
                            String message, InetAddress IPAddress,
                            int port) throws IOException {
        User user = new User("noNick", IPAddress, port);
        sendMessage(messageType, sender, message, user);
    }

    /**
     * Sends a message to a specified user.
     *
     * @param messageType the type of the message
     * @param sender the sender of the message
     * @param message the message
     * @param user the user to send the message to
     * @exception IOException if an error occurs
     */
    public void sendMessage(byte messageType, String sender,
                            String message, User user) throws IOException {
        InetAddress IPAddress = user.getIPAddress();
        int port = user.getPort();
        String sendMessage = "<" + sender + "> " + message;
        byte[] sendData = new byte[2 + sendMessage.length()];
        sendData[0] = messageType;
        sendData[1] = (byte) sendMessage.length();
        for (int i = 0, n = sendMessage.length(); i < n; i++) {
            sendData[i+2] = (byte) sendMessage.charAt(i);
        }
        System.out.println("Sending message of type " + (int) messageType + " to " + IPAddress + ":" + port);
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length,
                                                       IPAddress, port);
        serverSocket.send(sendPacket);
    }

    /**
     * Method to start the server from a command-line. Ex:
     * $ java Server // Creates a server listening to default port 7877
     * $ java Server 4444 // Creates a server listening to port 4444
     *
     * @param args first argument is used to specify which port the
     *             server should listen to
     * @exception IOException if an error occurs
     */
    public static void main(String[] args) throws IOException {
        int serverPort = 7877;
        try {
            serverPort = Integer.parseInt(args[0]);
        }
        catch (Exception e) {
            System.out.println("No port specified");
        }

        System.out.println("Server uses port: " + serverPort);
        Server serv = new Server(serverPort);
    }
}
