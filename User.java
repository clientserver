/*
 * @(#)User.java
 * Time-stamp: "2007-09-20 14:22:10 anton"
 */

import java.net.InetAddress;

/**
 * User
 *
 * @author "Anton Johansson" <anton.johansson@gmail.com>
 */

public class User {
    private InetAddress IPAdress;
    private int port;
    private String nickName;
    private Channel currentChannel;

    public User(String nickName, InetAddress IPAdress, int port) {
        this.nickName = nickName;
        this.IPAdress = IPAdress;
        this.port = port;
    }

    public void changeNick(String newNick) {
        this.nickName = newNick;
    }

    public void joinChannel(Channel channel) {
        this.currentChannel = channel;
    }

    public void leaveChannel() {
        this.currentChannel = null;
    }

    public void setPort(int port) {
        this.port = port;
    }

    // Get-methods
    public String getNick() {
        return this.nickName;
    }

    public InetAddress getIPAddress() {
        return this.IPAdress;
    }

    public int getPort() {
        return this.port;
    }

    public Channel getChannel() {
        return this.currentChannel;
    }
}
