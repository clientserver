/*
 * @(#)Channel.java
 * Time-stamp: "2007-10-02 15:24:38 anton"
 */

import java.util.Collection;
import java.util.Hashtable;

/**
 * Channel
 *
 * @author "Anton Johansson" <anton.johansson@gmail.com>
 */
public class Channel {
    private String channelName;
    private Hashtable<String, User> users;

    public Channel(String channelName) {
        this.channelName = channelName;
        this.users = new Hashtable<String, User>();
    }

    public void joinUser(User user) {
        users.put(user.getNick(), user);
    }

    public void leaveUser(User user) {
        users.remove(user.getNick());
    }

    public String getName() {
        return this.channelName;
    }

    public Collection<User> getUsers() {
        return this.users.values();
    }
}
