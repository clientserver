import java.io.*;
import java.util.*;

public class Library {
    
    private static BufferedReader read = new BufferedReader(new InputStreamReader(System.in));

    public static String prompt() {
        try {
            return read.readLine();
        }
        catch (IOException e) {
            System.err.println(e);
        }
        return null;
    }

    public static String prompt(String prompt) throws IOException {
        print(prompt);
        try {
            return read.readLine();
        }
        catch (IOException e) {
            System.err.println(e);
        }
        return null;
    }

    public static int promptInt(String prompt) throws IOException {
        print(prompt);
        try {
            return Integer.parseInt(read.readLine());
        }
        catch (IOException e) {
            System.err.println(e);
        }
        return -1;
    }

    private static void print(String prompt) {
        System.out.print(prompt);
    }
}
